<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Bottle Shooter</title>
        <script src="//cdn.jsdelivr.net/phaser/2.6.2/phaser.min.js"></script>
        <style>
            canvas { margin: 0 auto; }
        </style>
    </head>
    <body>

    <script type="text/javascript">

    window.onload = function() {

        var game = new Phaser.Game(1040, 1300, Phaser.AUTO, '', { preload: preload, create: create });

        function preload () {

            game.load.image('bg', 'imgs/bg.png');
            game.load.image('button_start', 'imgs/play.png');
			game.load.image('bottle1', 'imgs/bottle_1.png');
			game.load.image('bottle2', 'imgs/bottle_2.png');
			game.load.image('bottle3', 'imgs/bottle_3.png');
			game.load.image('bottle4', 'imgs/bottle_4.png');
			game.load.image('bottle5', 'imgs/bottle_5.png');

        }

function rnd(top) {
    return Math.floor((Math.random() * top) + 1);
}

var score = 0;
var max_score = 0;

var score_text;
var max_score_text;

function bot_onDown(sprite, pointer) {
	//alert('clicked !!!');
	sprite.visible = false;
	
	++score;
	
	update_score();
}

var bottles_list = [0]; // ignorar index 0
var bottles_lines = [0, 190, 580, 980]; // coordenada das linhas das prateleiras (nao se usa o index 0)

var button_start;

function game_over() {
    
    
    //alert('score = ' + score + '\nmax = ' + max_score);
    
    button_start.visible = true;
}

function update_score() {
    if (score > max_score) {max_score = score;}
    
    score_text.text = score;
    max_score_text.text = max_score;
    
}

function go_bottle(b, i) {
    
    b.x = -100;
    b.visible = true;
    
    var t = game.add.tween(b)

    t.to({ x: 1100 }, 3000, null, false, i * 400);

    if (i == (bottles_list.length - 1)) {
        t.onComplete.add(game_over, this);
    }
    
    t.start();
}

function run_bottles() {
    
    update_score();
    
    button_start.visible = false;
    score = 0;
    bottles_list.forEach(go_bottle);
}


        function create () {

            var style1 = { font: "32px Arial", fill: "#fff"};
            var style2 = { font: "bold 26px Arial", fill: "#fff"};

            game.add.text(100, 30, "score", style1);
            game.add.text(500, 30, "max", style1);
            
            score_text = game.add.text(200, 30, "0", style1);
            max_score_text = game.add.text(600, 30, "0", style1);




            game.add.sprite(0, 100, 'bg');
            
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

            button_start = game.add.button(game.world.centerX - 95, 400, 'button_start', run_bottles);
            
            bott_num = 1;
            var shelf = rnd(bottles_lines.length - 1);
            while(true) {
                for (b = 1; b <= 5; ++b) {

                    xt = -100;
                    yt = bottles_lines[shelf];
                    
        			var bot_tmp = game.add.sprite(xt, yt, 'bottle' + b);
        			bot_tmp.visible = false;
        			bot_tmp.inputEnabled = true;
        			bot_tmp.events.onInputDown.add(bot_onDown, this);

        			bottles_list.push(bot_tmp);

        			++bott_num;
        			++shelf;

        			if (shelf > (bottles_lines.length - 1)) {shelf = 1}
                }

                if (bott_num > 20) break;
            }

        }

    };

    </script>

    </body>
</html>